package org.digitallights.dynamicarray;

import org.junit.Ignore;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

import static org.junit.Assert.*;

public class ArrayListTest {
    private int sum = 0;

    @Test
    public void testAddElement(){
        Integer[] expected = new Integer[]{1};

        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(1);

        Integer[] actual = new Integer[1];
        actual = arrayList.toArray(actual);

        if (Arrays.compare(expected, actual) != 0)
            fail();
    }

    @Test
    public void testAddElementAtIndex() {
        Integer[] test = new Integer[] { 1, 2, 3 };
        Integer[] expected = new Integer[] { 1, 7, 2, 3 };

        ArrayList<Integer> arrayList = new ArrayList<>(List.of(test));
        arrayList.add(1, 7);

        Integer[] actual = new Integer[1];
        actual = arrayList.toArray(actual);

        if (Arrays.compare(expected, actual) != 0)
            fail();
    }

    @Test
    public void testAddAllElements() {
        Integer[] test = new Integer[] { 1, 2, 3 };
        Integer[] expected = new Integer[] { 1, 2, 3, 4, 5 };

        ArrayList<Integer> arrayList = new ArrayList<>(List.of(test));
        arrayList.addAll(List.of(4, 5));

        Integer[] actual = new Integer[1];
        actual = arrayList.toArray(actual);


        if (Arrays.compare(expected, actual) != 0)
            fail();
    }

    @Test
    public void testAddAllElementsAtIndex() {
        Integer[] test = new Integer[] { 1, 2, 3 };
        Integer[] expected = new Integer[] { 1, 4, 5, 2, 3 };

        ArrayList<Integer> arrayList = new ArrayList<>(List.of(test));
        arrayList.addAll(1, List.of(4, 5));

        Integer[] actual = new Integer[1];
        actual = arrayList.toArray(actual);


        if (Arrays.compare(expected, actual) != 0)
            fail();
    }

    @Test
    public void testIsEmpty() {
        Integer[] test = new Integer[] { 1, 2, 3 };

        ArrayList<Integer> arrayList = new ArrayList<>(List.of(test));

        if (arrayList.isEmpty())
            fail();

        ArrayList<Integer> arrayList1 = new ArrayList<>();

        if (!arrayList1.isEmpty())
            fail();
    }

    @Test
    public void testClear() {
        Integer[] test = new Integer[] { 1, 2, 3 };

        ArrayList<Integer> arrayList = new ArrayList<>(List.of(test));

        if (arrayList.isEmpty())
            fail();

        arrayList.clear();

        if (!arrayList.isEmpty())
            fail();
    }

    @Test
    public void testSwap() {
        Integer[] test1 = new Integer[] { 1, 2, 3 };
        Integer[] test2 = new Integer[] { 4, 5, 6, 7 };

        ArrayList<Integer> arrayList1 = new ArrayList<>(List.of(test1));
        ArrayList<Integer> arrayList2 = new ArrayList<>(List.of(test2));

        ArrayList.swap(arrayList1, arrayList2);

        Integer[] actual1 = new Integer[1];
        Integer[] actual2 = new Integer[1];

        actual1 = arrayList1.toArray(actual1);
        actual2 = arrayList2.toArray(actual2);

        if (arrayList1.size() != 4 || Arrays.compare(actual1, test2) != 0)
            fail();

        if (arrayList2.size() != 3 || Arrays.compare(actual2, test1) != 0)
            fail();
    }

    @Test
    public void testReserve() {
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.reserve(15);

        if (arrayList.capacity() != 15) fail();

        arrayList.addAll(
                List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 )
        );
    }

    @Test
    public void testShrinkToSize() {
        ArrayList<Integer> arrayList = new ArrayList<>();

        arrayList.shrinkToSize();

        if (arrayList.capacity() != 0)
            fail();
    }

    @Test
    public void testResize() {
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.addAll(List.of(1, 2, 3));

        if (!arrayList.resize(5))
            fail();

        if (arrayList.capacity() != 5)
            fail();

        if(!arrayList.resize(15))
            fail();

        if (arrayList.capacity() != 15)
            fail();

        if (arrayList.resize(0))
            fail();
    }

    @Test
    public void testRemove() {
        ArrayList<Integer> arrayList = new ArrayList<>(List.of(1, 2, 3));
        Integer removed = arrayList.remove(0);

        if (removed != 1 || arrayList.size() != 2)
            fail();

        if (!arrayList.remove(Integer.valueOf(3)))
            fail();
    }

    @Test
    public void testRemoveWithPredicate() {
        ArrayList<Integer> arrayList = new ArrayList<>(List.of(1, 2, 3));

        Predicate<Integer> predicate = num -> (num == 2);
        if (!arrayList.removeIf(predicate))
            fail();
    }

    @Test
    public void testGet() {
        ArrayList<Integer> arrayList = new ArrayList<>(List.of(1, 2, 3));
        int integer = arrayList.get(1);

        if (integer != 2)
            fail();
    }

    @Test
    public void testSet() {
        ArrayList<Integer> arrayList = new ArrayList<>(List.of(1, 2, 3));
        int prev = arrayList.set(1, 15);

        if (arrayList.get(1) != 15)
            fail();

        if (prev != 2)
            fail();
    }

    @Test
    public void testContains() {
        ArrayList<Integer> arrayList = new ArrayList<>(List.of(1, 2, 3));

        if (!arrayList.contains(1) ||
                !arrayList.contains(2) ||
                !arrayList.contains(3))
            fail();

        if (arrayList.contains(532))
            fail();
    }

    @Test
    public void testForEach() {
        ArrayList<Integer> arrayList = new ArrayList<>(List.of(1, 2, 3));
        arrayList.forEach(num -> sum += num);

        if (sum != 6)
            fail();
    }

    @Test
    public void testIndexOf() {
        ArrayList<Integer> arrayList = new ArrayList<>(List.of(1, 2, 3));

        if (arrayList.indexOf(2) != 1)
            fail();
    }

    @Test
    public void testLastIndexOf() {
        ArrayList<Integer> arrayList = new ArrayList<>(List.of(1, 2, 3, 4, 3, 5, 6));

        if (arrayList.lastIndexOf(3) != 4)
            fail();
    }

    @Test
    public void testConcatenate() {
        ArrayList<Integer> arrayList1 = new ArrayList<>(List.of(1, 2, 3, 4));
        ArrayList<Integer> arrayList2 = new ArrayList<>(List.of(5, 6, 7, 8));

        arrayList1.concatenate(arrayList2);
        Integer[] arr1 = new Integer[1];
        arr1 = arrayList1.toArray(arr1);

        Integer[] arr2 = new Integer[1];
        arr2 = List.of(1, 2, 3, 4, 5, 6, 7, 8).toArray(arr2);

        if (Arrays.compare(arr1, arr2) != 0)
            fail();
    }

    @Test
    public void testIterator() {
        // next(); hasNext();

        ArrayList<Integer> arrayList = new ArrayList<>(List.of(1, 2, 3));
        Iterator<Integer> iterator1 = arrayList.iterator();

        int count = 0;
        int[] res_arr = new int[3];

        while (iterator1.hasNext()){
            res_arr[count] = iterator1.next();
            count ++;
        }

        assertEquals(3, count);
        assertArrayEquals(new int[] {1, 2, 3}, res_arr);

        // remove()

        Iterator<Integer> iterator2 = arrayList.iterator();
        iterator2.next();
        iterator2.remove();

        Integer[] arr = new Integer[1];
        arr = arrayList.toArray(arr);

        assertArrayEquals(new Integer[] {2, 3}, arr);

        // forEachRemaining()

        ArrayList<Integer> arrayList1 = new ArrayList<>(List.of(1, 2, 3));
        Iterator<Integer> iterator3 = arrayList1.iterator();

        sum = 0;
        iterator3.forEachRemaining((num) -> sum += num);

        assertEquals(6, sum);
    }
}
package org.digitallights.dynamicarray;

import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * A custom implementation of the ArrayList class in java.util package.
 *
 * @param <T> The type of objects in the array.
 */
public class ArrayList<T> {
    /**
     * The size of the internal buffer
     */
    private int capacity;
    /**
     * The default size of the internal buffer
     */
    private static final int DEFAULT_CAPACITY = 10;
    /**
     * The resizing factor for the buffer
     */
    private static final int RESIZE_FACTOR = 2;
    /**
     * The number of elements in the buffer
     */
    private int size;
    /**
     * The buffer itself
     */
    private Object[] array;

    /**
     * An iterator implementation for the ArrayList class.
     */
    private class Itr implements Iterator<T> {
        private int position;
        private int lastPosition;

        public Itr() {
            position = 0;
            lastPosition = 0;
        }

        @Override
        public boolean hasNext() {
            return position != size;
        }

        @Override
        public T next() {
            T res = (T) array[position];
            lastPosition = position;
            position ++;
            return res;
        }

        @Override
        public void remove() {
            ArrayList.this.remove(lastPosition);
            position --;
        }

        @Override
        public void forEachRemaining(Consumer<T> action) {
            while (hasNext()) {
                action.accept(next());
            }
        }
    }

    /**
     * Constructs an empty list with an initial capacity of ten.
     */
    public ArrayList() {
        capacity = DEFAULT_CAPACITY;
        size = 0;
        array = new Object[capacity];
    }

    /**
     * Constructs an empty list with the specified initial capacity.
     *
     * @param capacity the initial capacity of the list
     */
    public ArrayList(int capacity) {
        this.capacity = capacity;
        size = 0;
        array = new Object[capacity];
    }

    /**
     * Constructs a list containing the elements of the specified collection,
     * in the order they are returned by the collection's iterator.
     *
     * @param collection the collection whose elements are to be placed into this list
     */
    public ArrayList(Collection<? extends T> collection) {
        capacity = RESIZE_FACTOR * collection.size();
        array = new Object[capacity];

        size = 0;
        for (T c : collection) {
            array[size++] = c;
        }
    }

    /**
     * Appends the specified element to the end of this list.
     *
     * @param e element to be appended to this list
     */
    public void add(T e) {
        if (size == capacity)
            resizeBufferCapacity();

        array[size ++] = e;
    }

    /**
     * Inserts the specified element at the specified position in this list.
     * Shifts the element currently at that position (if any) and any
     * subsequent elements to the right (adds one to their indices).
     *
     * @param index index at which the specified element is to be inserted
     * @param element element to be inserted
     *
     * @exception IndexOutOfBoundsException if the index is out of range
     */
    public void add(int index, T element) throws IndexOutOfBoundsException{
        Objects.checkIndex(index, size);

        if (size == capacity)
            resizeBufferCapacity();

        Object[] temp_arr = new Object[size + 1];

        System.arraycopy(array, 0, temp_arr, 0, index);
        temp_arr[index] = element;
        System.arraycopy(array, index, temp_arr, index + 1, size - index);

        size ++;
        System.arraycopy(temp_arr, 0, array, 0, size);
    }

    /**
     * Appends all the elements in the specified collection to the end of this list,
     * in the order that they are returned by the specified collection's Iterator.
     * The behavior of this operation is undefined if the specified collection is modified
     * while the operation is in progress. (This implies that the behavior of this call is
     * undefined if the specified collection is this list, and this list is nonempty.)
     *
     * @param c collection containing elements to be added to this list
     */
    public void addAll(Collection<? extends T> c) {
        for (T obj : c)
            add(obj);
    }

    /**
     * Inserts all the elements in the specified collection into this list,
     * starting at the specified position. Shifts the element currently at that position
     * (if any) and any subsequent elements to the right (increases their indices).
     * The new elements will appear in the list in the order that they are returned by the
     * specified collection's iterator.
     *
     * @param index index at which to insert the first element from the specified collection
     * @param c collection containing elements to be added to this list
     * @exception IndexOutOfBoundsException if invalid index is passed
     */
    public void addAll(int index, Collection<? extends T> c) throws IndexOutOfBoundsException{
        for (T obj : c) {
            add(index, obj);
            index ++;
        }
    }

    /**
     * Returns an array containing all the elements in this list in proper sequence
     * (from first to last element); the runtime type of the returned array is that
     * of the specified array. If the list fits in the specified array, it is returned therein.
     * Otherwise, a new array is allocated with the runtime type of
     * the specified array and the size of this list.
     *
     * @param a array used to get the needed type of elements
     * @return an array containing the elements of the list
     */
    public T[] toArray(T[] a) {
        return (T[]) Arrays.copyOf(this.array, this.size, a.getClass());
    }

    /**
     * Returns the number of elements in this list.
     * @return the number of elements in this list.
     */
    public int size() {
        return size;
    }

    /**
     * Returns true if this list contains no elements.
     * @return true if this list contains no elements
     */
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * Removes all the elements from this list.
     */
    public void clear() {
        size = 0;
        array = new Object[capacity];
    }

    /**
     * Returns the size of the underlying buffer.
     * @return the size of the underlying buffer.
     */
    public int capacity() {
        return capacity;
    }
    /**
     * Exchanges the contents of two containers, without the need to perform a full copy.
     * @param arrayList1 First container
     * @param arrayList2 Second container
     * @param <E> the type of elements the passed array lists hold.
     */
    public static <E> void swap(ArrayList<E> arrayList1, ArrayList<E> arrayList2) {
        Object[] temp;
        int temp_size, temp_capacity;

        temp = arrayList1.array;
        temp_size = arrayList1.size;
        temp_capacity = arrayList1.capacity;

        arrayList1.array = arrayList2.array;
        arrayList1.size = arrayList2.size;
        arrayList1.capacity = arrayList2.capacity;

        arrayList2.array = temp;
        arrayList2.size = temp_size;
        arrayList2.capacity = temp_capacity;
    }

    /**
     * Increases the capacity of this ArrayList instance,
     * if necessary, to ensure that it can hold at least the number of
     * elements specified by the minimum capacity argument.
     *
     * @param minCapacity the minimum capacity that has to be ensured
     */
    public void reserve(int minCapacity) {
        if (capacity < minCapacity && minCapacity >= 0) {
            Object[] temp = array;

            array = new Object[minCapacity];
            capacity = minCapacity;

            System.arraycopy(temp, 0, array, 0, this.size);
        }
    }

    /**
     * Set the size of the capacity to a specified value.
     *
     * @param capacity the capacity to which the buffer must be resized
     * @return true if the buffer is successfully resized
     */
    public boolean resize(int capacity) {
        if (capacity < size || capacity < 0)
            return false;

        if (capacity != this.capacity) {
            Object[] temp = array;

            array = new Object[capacity];
            this.capacity = capacity;

            System.arraycopy(temp, 0, array, 0, this.size);
        }

        return true;
    }

    /**
     * Trims capacity of the underlying buffer to the number of contained elements.
     */
    public void shrinkToSize() {
        if (capacity != size) {
            Object[] temp = array;

            array = new Object[this.size];
            capacity = this.size;

            System.arraycopy(temp, 0, array, 0, this.size);
        }
    }

    /**
     * Removes the element at the specified position in this list.
     * @param index the index of the element for removal.
     * @return the removed element.
     * @throws IndexOutOfBoundsException if invalid index was passed.
     */
    public T remove(int index) throws IndexOutOfBoundsException{
        Objects.checkIndex(index, size);

        Object[] temp = new Object[size - 1];

        System.arraycopy(array, 0, temp, 0, index);
        T res = (T) array[index];
        System.arraycopy(array, index + 1, temp, index, size - (index + 1));

        array = temp;
        size --;

        return res;
    }

    /**
     * Removes the first occurrence of the specified element from this list, if it is present.
     * @param o the object to be removed.
     * @return true if the object is successfully removed.
     */
    public boolean remove(Object o) {
        boolean res = false;

        int idx = 0;
        for (Object obj : array) {
            if (o.equals(obj)) {
                res = true;
                remove(idx);
                break;
            }
            idx ++;
        }
        return res;
    }

    /**
     * Remove all elements that satisfy the specified predicate.
     * @param filter the predicate specified.
     * @return true if at least 1 element was removed.
     */
    public boolean removeIf(Predicate<T> filter) {
        boolean res = false;
        boolean[] idxs_for_removal = new boolean[size];

        for (int idx = 0; idx < size; idx ++) {
            if (filter.test((T) array[idx])){
                res = true;
                idxs_for_removal[idx] = true;
            }
        }

        for (int i = 0; i < idxs_for_removal.length; i ++)
            if (idxs_for_removal[i])
                remove(i);

        return res;
    }

    /**
     * Returns the element at the specified position in this list.
     *
     * @param index index of the element to return
     * @return the element at the specified position in this list
     * @throws IndexOutOfBoundsException if the index is out of range
     */
    public T get(int index) throws IndexOutOfBoundsException {
        Objects.checkIndex(index, size);
        return (T) array[index];
    }

    /**
     * Replaces the element at the specified position in this list with the specified element.
     *
     * @param index index of the element to replace
     * @param obj element to be stored at the specified position
     * @throws IndexOutOfBoundsException if the index is out of range
     * @return the old element at the specified index
     */
    public T set(int index, T obj) throws IndexOutOfBoundsException {
        Objects.checkIndex(index, size);
        T old = (T) array[index];
        array[index] = obj;
        return old;
    }

    /**
     * Returns true if this list contains the specified element.
     * @param obj element whose presence in this list is to be tested
     * @return true if this list contains the specified element
     */
    public boolean contains(Object obj) {
        boolean res = false;

        for (int i = 0; i < size; i ++)
            if (array[i].equals(obj)) {
                res = true;
                break;
            }

        return res;
    }

    /**
     * Performs the given action for each element of the list.
     *
     * @param action The action to be performed on each element.
     */
    public void forEach(Consumer<T> action) {
        for (int i = 0; i < size; i ++) {
            action.accept((T) array[i]);
        }
    }

    /**
     * Returns the index of the first occurrence of the specified element in this list
     * @param obj element to search for
     * @return the index of the first occurrence of the specified element in this list,
     * or -1 if this list does not contain the element
     */
    public int indexOf(Object obj) {
        for (int i = 0; i < size; i ++)
            if (obj.equals(array[i]))
                return i;

        return -1;
    }

    /**
     * Returns the index of the last occurrence of the specified element in this list
     * @param obj element to search for
     * @return the index of the last occurrence of the specified element in this list,
     * or -1 if this list does not contain the element
     */
    public int lastIndexOf(Object obj) {
        for (int i = size - 1; i >= 0; i --)
            if (obj.equals(array[i]))
                return i;

        return -1;
    }

    /**
     * Concatenates the contents of another array to the current one.
     * @param arrayList the source array for the concatenation.
     */
    public void concatenate(final ArrayList<T> arrayList) {
        capacity += arrayList.capacity;
        Object[] new_arr = new Object[capacity];

        System.arraycopy(array, 0, new_arr, 0, size);
        System.arraycopy(arrayList.array, 0, new_arr, size, arrayList.size);

        size += arrayList.size;
        array = new_arr;
    }

    /**
     * Resize the buffer capacity when necessary.
     */
    private void resizeBufferCapacity() {
        capacity = RESIZE_FACTOR * size;
        Object[] new_arr = new Object[capacity];

        System.arraycopy(array, 0, new_arr, 0, size);
        array = new_arr;
    }

    /**
     * Returns an iterator over the elements in this list in proper sequence.
     *
     * @return an iterator over the elements in this list in proper sequence.
     */
    public Iterator<T> iterator() {
        return new Itr();
    }
}

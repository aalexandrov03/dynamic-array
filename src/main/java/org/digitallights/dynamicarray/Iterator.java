package org.digitallights.dynamicarray;

import java.util.function.Consumer;

public interface Iterator<E> {
    boolean hasNext();
    E next();
    void remove();
    void forEachRemaining(Consumer<E> action);
}
